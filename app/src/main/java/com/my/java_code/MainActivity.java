package com.my.java_code;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import es.uc3m.smds.android.weka.ModelInterface;
import weka.core.Instances;
import weka.core.RevisionHandler;
import es.uc3m.smds.android.utils.Util;
import es.uc3m.smds.android.weka.ModelJ48;

import org.jf.dexlib2.DexFileFactory;
import org.jf.dexlib2.iface.DexFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Vector;

import jadx.api.JadxDecompiler;
import weka.core.converters.ArffLoader;
public class MainActivity extends Activity {

    String TAG = "DOOOOOOOOOOD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView t1 = (TextView) findViewById(R.id.text1);
        String s = Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE).toString() + "/test.apk";
        t1.setText(s);
        File f = new File(s);
        if (f.exists()) {
            Toast.makeText(MainActivity.this, "Yaaay!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, ":-(", Toast.LENGTH_LONG).show();
        }
    }

    public void clickme(View v) {

        new apktodex(this).execute();
    }

    //*****************************************APK to DEX*********************************************
    private class apktodex extends AsyncTask<Void, Void, Integer> {
        Context c;
        DexFile d = null;

        public apktodex(Context a) {
            c = a;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Log.d(TAG, "Hi From Inside");
            int r;

            try {
                String s = Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE).toString();
                d = DexFileFactory.loadDexFile(s + "/test.apk", 16);
                DexFileFactory.writeDexFile(s + "/opt_classes.dex", d);
                Log.d(TAG, "DONE!");
                r = 1;
            } catch (IOException e) {
                e.printStackTrace();
                r = 0;
            }
            return r;
        }

        @Override
        protected void onPostExecute(Integer b) {
            TextView t = (TextView) findViewById(R.id.text2);
            if (b == 1) {
                t.setText("APK to DEX is DONE :)");
            } else {
                t.setText("APK to DEX is NOT done :(");
            }
        }
    }
    //**************************************DEX to Jar*******************************************
/*    public void clickme2(View v){

        new dextojar().execute();
    }

    private class dextojar extends AsyncTask <Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            boolean reuseReg = false; // reuse register while generate java .class file
            boolean topologicalSort1 = false; // same with --topological-sort/-ts
            boolean topologicalSort = false; // sort block by topological, that will generate more readable code
            boolean verbose = true; // show progress
            boolean debugInfo = false; // translate debug info
            boolean printIR = false; // print ir to System.out
            boolean optimizeSynchronized = true; // Optimise-synchronised

            File PerAppWorkingDirectory = Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE);
            File file = new File(PerAppWorkingDirectory + "/" + "jarfile" + ".jar");
            File dexFile = new File(PerAppWorkingDirectory + "/opt_classes.dex");

            if (dexFile.exists() && dexFile.isFile()) {
                //DexExceptionHandlerMod dexExceptionHandlerMod = new DexExceptionHandlerMod();
                try {
                    DexFileReader reader = new DexFileReader(dexFile);
                    Dex2jar dex2jar = Dex2jar.from(reader).reUseReg(reuseReg)
                            .topoLogicalSort(topologicalSort || topologicalSort1).skipDebug(!debugInfo)
                            .optimizeSynchronized(optimizeSynchronized).printIR(printIR).verbose(verbose);
                    dex2jar.to(file);
                    return 1;
                } catch (Exception e) {
                }
            }
                return 0;
        }

        @Override
        protected void onPostExecute(Integer b){
            TextView t = (TextView)findViewById (R.id.text2);
            if (b==1) {
                t.setText("It's DONE!");
            } else {
                t.setText("It's NOT done :(");
            }
        }
    }*/

    //**************************************DEX to Java*******************************************
    public void clickme3(View v) {

        new dextojava().execute();
    }

    private class dextojava extends AsyncTask<Void, Void, Integer> {
        int r;

        @Override
        protected Integer doInBackground(Void... voids) {
            try {
                File dexInputFile = new File(Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE)
                        + "/opt_classes.dex");
                JadxDecompiler jadx = new JadxDecompiler();
                jadx.setOutputDir(Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE));
                jadx.loadFile(dexInputFile);
                jadx.saveSources();
                r = 0;
            } catch (Exception | StackOverflowError e) {
                r = 1;
            }
            return r;
        }

        @Override
        protected void onPostExecute(Integer b) {
            TextView t = (TextView) findViewById(R.id.text2);
            if (b == 1) {
                t.setText("Java Code is Ready :)");
            } else {
                t.setText("Java Code is Ready :)");
            }
        }
    }

    //**************************************Java to TXT*******************************************
    public void get_java_code(View v) {
        String[] ext = new String[]{"java"};
        String sourceFilePath = Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE) +
                "/android";
        File dir = new File(sourceFilePath);
        int count = 0;
        TextView t = (TextView) findViewById(R.id.text2);
        List<File> files = (List<File>) FileUtils.listFiles(dir, ext, true);
        for (int i = 0; i < files.size(); i++) {
            try {
                String sourceFilePath2 = files.get(i).getCanonicalPath();
                FileInputStream fs;
                int ch;
                StringBuilder str = new StringBuilder();
                String sourceCodeText = "";
                fs = new FileInputStream(new File(sourceFilePath2));
                while ((ch = fs.read()) != -1) {
                    str.append((char) ch);
                }
                sourceCodeText = str.toString();
                PrintWriter out = new PrintWriter(Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE)
                        + "/source_code/file" + count + ".txt");
                out.println(sourceCodeText);
                fs.close();
                out.close();
                count = count + 1;
                Log.d(TAG, Integer.toString(count));

            } catch (IOException ignored) {
            }
        }
        count_intents();
        t.setText("Source Code is Ready :)");
    }

    //**************************** ITERATE THROUGH GENERATED TXT FILES *****************************
    public void count_intents() {
        String[] ext = new String[]{"txt"};
        String sourceFilePath = Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE) +
                "/source_code";
        File dir = new File(sourceFilePath);
        int count = 0;
        List<File> files = (List<File>) FileUtils.listFiles(dir, ext, true);
        for (int i = 0; i < files.size(); i++) {
            try {
                String sourceFilePath2 = files.get(i).getCanonicalPath();
                File txtfile = new File(sourceFilePath2);
                BufferedReader tfile = new BufferedReader(new FileReader(txtfile));
                String line = "";
                while ((line = tfile.readLine()) != null) {
                    if (line.contains("intent")) {
                        count = count + 1;
                    }
                }
                Log.d(TAG, Integer.toString(i));
            } catch (IOException e) {

            }
        }
        Log.d(TAG, "Number of intents are: " + Integer.toString(count));
    }

    //*************************** TRAIN J48 CLASSIFIER AND SAVE THE MODEL **************************
 /*   public void train_and_save(View v) throws IOException {
        String TAG = "DOOOOOOOOOOD";
        TextView t = (TextView) findViewById(R.id.text2);
        String sourceFilePath = Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE) + "/data.arff";
        Log.d(TAG, sourceFilePath);
        BufferedReader reader = new BufferedReader(new FileReader(sourceFilePath));
        ArffLoader.ArffReader arff = new ArffLoader.ArffReader(reader);
        Instances data = arff.getData();
        Log.d(TAG, "[J48] Generating model...");
        ModelInterface c = new ModelJ48();
        Vector<RevisionHandler> model = c.generateModel(data, true);

        Log.d(TAG, "[J48] Saving model...");
        String sourceFilePath2 = Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE) + "/j48.model";
        File fModel_out = new File(sourceFilePath2);
        Util.saveModel(model, fModel_out);
        Log.d(TAG, "[J48] Model Saved!");
        t.setText("The model has been saved!");
    }*/

    //*************************** TEST THE MODEL WITH NEW DATA **************************
    public void test(View v) throws IOException {

        TextView t = (TextView) findViewById(R.id.text2);
        String sourceFilePath = Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE) + "/data.arff";
        String modelFilePath = Environment.getExternalStoragePublicDirectory(DOWNLOAD_SERVICE) + "/j48.model";

        BufferedReader reader = new BufferedReader(new FileReader(sourceFilePath));
        ArffLoader.ArffReader arff = new ArffLoader.ArffReader(reader);
        Instances data = arff.getData();

        File f = new File(modelFilePath);
        Vector<RevisionHandler> model = Util.readModel(f);

        Log.d(TAG, "[J48] Predicting...");
        ModelInterface c = new ModelJ48();
        String s = c.predict(model, data);

        Log.d(TAG, "[J48] End of Prediction");
        Log.d(TAG, s);
        t.setText("Testing is Done!");
    }
}