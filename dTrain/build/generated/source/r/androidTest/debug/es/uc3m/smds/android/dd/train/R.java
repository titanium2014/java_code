/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package es.uc3m.smds.android.dd.train;

public final class R {
	public static final class dimen {
		public static final int activity_horizontal_margin = 0x7f040000;
		public static final int activity_vertical_margin = 0x7f040001;
	}
	public static final class drawable {
		public static final int ic_launcher = 0x7f020000;
		public static final int radar = 0x7f020001;
		public static final int radar_icon = 0x7f020002;
	}
	public static final class id {
		public static final int action_settings = 0x7f070002;
		public static final int textview = 0x7f070000;
		public static final int toggleButton = 0x7f070001;
	}
	public static final class layout {
		public static final int activity_ddprepare = 0x7f030000;
		public static final int main_detection = 0x7f030001;
		public static final int main_monitoring = 0x7f030002;
	}
	public static final class menu {
		public static final int ddprepare = 0x7f060000;
	}
	public static final class string {
		public static final int action_settings = 0x7f050000;
		public static final int actualizar = 0x7f050001;
		public static final int app_name = 0x7f050002;
		public static final int envia_histograma = 0x7f050003;
		public static final int enviando_histograma = 0x7f050004;
		public static final int error_enviar = 0x7f050005;
		public static final int error_installation = 0x7f050006;
		public static final int error_sd = 0x7f050007;
		public static final int error_strace = 0x7f050008;
		public static final int exito_enviar = 0x7f050009;
		public static final int ficheros = 0x7f05000a;
		public static final int hello_world = 0x7f05000b;
		public static final int local_service_label = 0x7f05000c;
		public static final int local_service_name = 0x7f05000d;
		public static final int local_service_started = 0x7f05000e;
		public static final int monitoring_of = 0x7f05000f;
		public static final int monitoring_on = 0x7f050010;
		public static final int parar_todo = 0x7f050011;
		public static final int procesos = 0x7f050012;
		public static final int title_activity_ddprepare = 0x7f050013;
		public static final int toggle_turn_off = 0x7f050014;
		public static final int toggle_turn_on = 0x7f050015;
	}
}
