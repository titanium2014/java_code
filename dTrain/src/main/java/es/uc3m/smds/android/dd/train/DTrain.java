package es.uc3m.smds.android.dd.train;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Vector;

import weka.core.Instances;
import weka.core.RevisionHandler;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import es.uc3m.smds.android.utils.Util;
import es.uc3m.smds.android.weka.ModelInterface;
import es.uc3m.smds.android.weka.ModelJ48;
import es.uc3m.smds.android.weka.ModelKmeans;
import es.uc3m.smds.android.weka.ModelNaiveBayes;

public class DTrain extends Activity {

	static final String TAG = "DTrain";
	
	AsyncTask<File, Object, Object> train;

	private TextView vista;
	File working_directory = null;

	String TEST;
	
	private final BroadcastReceiver registroUIReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle extras = intent.getExtras();
			int scale = extras.getInt(BatteryManager.EXTRA_SCALE);
			int level = extras.getInt(BatteryManager.EXTRA_LEVEL);
			Log.w("EXP", "Bateria: " + level + " / " + scale);

			Util.wekalog("Bateria: " + level + " / " + scale + "\n\n");
		
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ddprepare);

		TEST =  ((DetectionApplication)getApplication()).getTest();
		
		Log.i(TAG, "Starting activity");
		
        setContentView(R.layout.main_detection);
    	vista = (TextView) findViewById(R.id.textview); 

    	working_directory = new File(Environment.getExternalStorageDirectory(), "/StraceExperiments");
    	
    	File training_file = new File(working_directory, ((DetectionApplication)getApplication()).getInputFile());
    	if(!training_file.exists()){

        	Util.copyAssetToSdcardDir(getApplicationContext(), ((DetectionApplication)getApplication()).getInputFile(), "StraceExperiments");
    	}
    	
    

		registerReceiver(registroUIReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

		String mesg = TAG + " - " +  TEST + " - Start Time: " + String.valueOf(System.currentTimeMillis() - ((DetectionApplication)getApplication()).startTime);
		Util.wekalog(new Date().toString() + " : \n" + mesg + "\n");
		Log.i(TAG, mesg);

		train = new AsyncTask<File, Object, Object>(){

		    @Override
		    protected void onPreExecute(){
		    	vista.setKeepScreenOn(true);
		    }
			
			@Override
			protected Object doInBackground(File... params) {
				
				/*
				 * Training and Testing
				 */

				Log.v(TAG, ((DetectionApplication)getApplication()).getTest());
				publishProgress(((DetectionApplication)getApplication()).getTest() + " - Time: " + ((String.valueOf((int)(System.currentTimeMillis() - ((DetectionApplication)getApplication()).startTime)/1000))) + " sec.", String.valueOf( 
														((DetectionApplication)getApplication()).getCurrentTest()*
														((DetectionApplication)getApplication()).getNumCurrentModel()+
														((DetectionApplication)getApplication()).getCurrentTest()
													), ((DetectionApplication)getApplication()).getNumTest()*((DetectionApplication)getApplication()).getNumModels());
				//
				String WEKA_TRAINING_FILE = params[0].getName();
		    	String MODEL_PATH = working_directory.getAbsolutePath();//private static final String MODEL_PATH = "modelset";

				File fTraining_in = null;

				fTraining_in = params[0];

				Instances data = Util.loadData(fTraining_in);

				switch (((DetectionApplication)getApplication()).getCurrentModel()) {
				case J48:

					Log.v(TAG, "[J48] Using " + WEKA_TRAINING_FILE);
					
					//J48
					{
						System.out.println("************************ J48 ************************");

						Log.v(TAG, "[J48] Generating model...");
						ModelInterface c = new ModelJ48();
						Vector<RevisionHandler> model = c.generateModel(data, true);

						Log.v(TAG, "[J48] Saving model...");
						File fModel_out = new File(MODEL_PATH + "/" + WEKA_TRAINING_FILE + "." + c.getTag() + ".model");
						Util.saveModel(model, fModel_out);
					}
					
					break;
				case KMEANS:
					
					Log.v(TAG, "[Kmeans] Using " + WEKA_TRAINING_FILE);
					
					//Kmeans
					{
						Log.v(TAG, "************************ Kmeans ************************");
						
						Log.v(TAG, "[Kmeans] Generating model...");
						ModelInterface c = new ModelKmeans();
						Vector<RevisionHandler> model = c.generateModel(data, true);
	
						Log.v(TAG, "[Kmeans] Saving model...");
						File fModel_out = new File(MODEL_PATH + "/" + WEKA_TRAINING_FILE + "." + c.getTag() +  ".model");
						Util.saveModel(model, fModel_out);
					}
					break;

				case NAIVEBAYES:

					Log.v(TAG, "[NaiveBayes] Using " + WEKA_TRAINING_FILE);
					
					//NaiveBayes
					{
						System.out.println("************************ NaiveBayes ************************");

						Log.v(TAG, "[NaiveBayes] Generating model...");
						ModelInterface c = new ModelNaiveBayes();
						Vector<RevisionHandler> model = c.generateModel(data, true);

						Log.v(TAG, "[NaiveBayes] Saving model...");
						File fModel_out = new File(MODEL_PATH + "/" + WEKA_TRAINING_FILE + "." + c.getTag() + ".model");
						Util.saveModel(model, fModel_out);
					}
					break;

				default:
					Log.e(TAG, "No model especified");
					break;
				}
				
				return null;
			}

			@Override
			protected void onProgressUpdate(Object... values) {
				vista.setText(values[0] + " (" + values[1] + " / " + values[2] + ")");
			}

			@Override
			protected void onPostExecute(Object o) {
				vista.setKeepScreenOn(false);
//				
//				if(((DetectionApplication)getApplication()).thereAreMoreTests()){
//					Intent test = new Intent(getApplicationContext(), DTrain.class);
//					startActivity(test);
//				}else{
//					String mesg = TAG + " - " +  TEST + " - End Time: " + String.valueOf(System.currentTimeMillis() - ((DetectionApplication)getApplication()).startTime);
//					vista.setText(mesg);
//				}
				
				
			}
		};
		
//		try{
			train.execute(training_file);
//		}catch (StackOverflowError e){
//			String mesg2 = TAG + " - " +  TEST + " - End Time due to StackOverflowError: " + String.valueOf(System.currentTimeMillis() - ((DetectionApplication)getApplication()).startTime);
//			Log.e(TAG, mesg2);
//			
//			try {
//				out.write(mesg2 + "\n");
//				
//			} catch (IOException e2) {
//				Log.e(TAG, "Error writing BatteryLog file.", e2);
//			}
//		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ddprepare, menu);
		return true;
	}
	
	@Override
	protected void onStart() {
		Log.v(TAG, "onStart");
		super.onStart();
	}

	
	@Override
	protected void onStop() {
		Log.v(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	protected void onPause() {
		
		String mesg = TAG + " - " +  TEST + " - End Time: " + String.valueOf(System.currentTimeMillis() - ((DetectionApplication)getApplication()).startTime);
		Util.wekalog(new Date().toString() + " : " + mesg + " \n");
		Log.i(TAG, mesg);
		
		unregisterReceiver(registroUIReceiver);

		//((DetectionApplication)getApplication()).increaseTest();
		
		Log.v(TAG, "onPause");
		super.onPause();

		train = null;
		finish();
	}
	
	@Override
	protected void onResume() {
		Log.v(TAG, "onResume");
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		Log.v(TAG, "onDestroy");
		super.onDestroy();
	}

}
