package es.uc3m.smds.android.dd.train;

import android.app.Application;

public class DetectionApplication extends Application{

	public final String TRAINING_RAW_INPUT_FILE_NAME_GOODWARE = "goodware.strace";
	public final String TRAINING_RAW_INPUT_FILE_NAME_MALWARE = "malware.strace";
	public final String PREDICTING_RAW_INPUT_FILE_NAME = "test.strace";

	/*
		#Type      |    #Apps    |  #Events | #Throttle    : Mean
		Goodware     |    10    |    5000    |    1000        : 180.43902352780567513172
		Goodware     |    35    |    1000    |    5000        : 453.91845639242371610254
		Goodware     |    50    |    5000    |    1000        : 307.62907384457447438994
		Malware        |    10    |    5000    |    1000        : 112.39483515630732850447
		Malware        |    50    |    1000    |    5000        : 128.66660844739102454124
		Malware        |    50    |    5000    |    1000        : 161.90699332360925274199
		-----------------------------------------------------------------------------------
		-----------        |    ----    |    -------    |    -------        : 224.159165115
	 */
	
	public final int vector_length = 225;
	
	public final long startTime = System.currentTimeMillis();

	
	public boolean thereAreMoreTests(){
		
		if(getNumCurrentModel() >= getNumModels() 
				&&
			getCurrentTest() >= getNumTest()){
			
				return false;
			
		}
		
		return true;
	}
	
	public enum MODELS {
	    J48, KMEANS, NAIVEBAYES
	}

	private int CURRENT_MODEL = 0; 

//	public int getCurrentModel(){
//		return CURRENT_MODEL;
//	}
	
	public MODELS getCurrentModel(){
		return MODELS.values()[CURRENT_MODEL];
	}
	

	public int getNumCurrentModel(){
		return CURRENT_MODEL;
	}
	
	public void increaseModel(){
		CURRENT_MODEL++;
	}

	public int getNumModels(){
		return MODELS.values().length;
	}
	
	private int CURRENT_TEST = 0;
	
	private String[] INPUT_FILE_NAME = new String[]{
			"mockVectors.10.10.arff"
//			"mockVectors.100.10.arff",
//			"mockVectors.100.100.arff", 
//			"mockVectors.100.200.arff", 
//			"mockVectors.100.300.arff", 
//			"mockVectors.100.400.arff", 
//			"mockVectors.1000.10.arff", 
//			"mockVectors.1000.100.arff", 
//			"mockVectors.1000.200.arff", 
//			"mockVectors.1000.300.arff", 
//			"mockVectors.1000.400.arff", 
//			"mockVectors.10000.10.arff", 
//			"mockVectors.10000.100.arff", 
//			"mockVectors.10000.200.arff", 
//			"mockVectors.10000.300.arff", 
//			"mockVectors.10000.400.arff", 
//			"mockVectors.100000.10.arff", 
//			"mockVectors.100000.100.arff", 
//			"mockVectors.100000.200.arff", 
//			"mockVectors.100000.300.arff", 
//			"mockVectors.100000.400.arff", 
//			"mockVectors.200.10.arff", 
//			"mockVectors.200.100.arff", 
//			"mockVectors.200.200.arff", 
//			"mockVectors.200.300.arff", 
//			"mockVectors.200.400.arff", 
//			"mockVectors.50.10.arff", 
//			"mockVectors.50.100.arff", 
//			"mockVectors.50.200.arff", 
//			"mockVectors.50.300.arff", 
//			"mockVectors.50.400.arff", 
//			"mockVectors.500.10.arff", 
//			"mockVectors.500.100.arff", 
//			"mockVectors.500.200.arff", 
//			"mockVectors.500.300.arff", 
//			"mockVectors.500.400.arff"
	};
	

	public String getTest(){
		
		return "[TrainPhone][" + getCurrentModel() + "] " + INPUT_FILE_NAME[CURRENT_TEST];
	}
	

	public int getCurrentTest(){
		return CURRENT_TEST;
	}
	
	public String getInputFile(){
		
		return INPUT_FILE_NAME[CURRENT_TEST];
	}
	
	public int getNumTest(){
		return INPUT_FILE_NAME.length;
	}
	
	public void increaseTest(){
		
		if(getCurrentTest() >= getNumTest()){
			CURRENT_TEST=0;
			increaseModel();
		}else{
			CURRENT_TEST++;
		}
	}
}
