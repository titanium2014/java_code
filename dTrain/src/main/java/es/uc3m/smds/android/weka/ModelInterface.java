package es.uc3m.smds.android.weka;

import java.util.Vector;

import weka.classifiers.Classifier;
import weka.core.Instances;
import weka.core.RevisionHandler;

public abstract class ModelInterface {
	
	abstract public String getTag();
	
	//Model
	abstract public Vector<RevisionHandler> generateModel(Instances data);
	abstract public Vector<RevisionHandler> generateModel(Instances data, boolean evaluateWithCrossValidation);
	
	//Evaluate model
	abstract public void evaluate(Vector<RevisionHandler> model, Instances test);
	abstract public void evaluate_crossvalidation(RevisionHandler cl, Instances data);
	
	//Prediction
	abstract public String predict(Vector<RevisionHandler> model, Instances data);
}
