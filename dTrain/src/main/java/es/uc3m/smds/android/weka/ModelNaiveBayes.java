package es.uc3m.smds.android.weka;

import java.util.Vector;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Debug.Random;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.RevisionHandler;
import es.uc3m.smds.android.utils.Util;


public class ModelNaiveBayes extends ModelInterface{

	private final String TAG;
	
	public ModelNaiveBayes(){
		TAG = "NaiveBayes";
	}
	
	public String getTag(){
		return TAG;
	}
	

	@Override
	public Vector<RevisionHandler> generateModel(Instances data) {
		return generateModel(data, false);
	}
	
	/*
	 * ************************************************************************************************
	 * ******************************************** Model *********************************************
	 * ************************************************************************************************
	 */
	
	@Override
	public Vector<RevisionHandler> generateModel(Instances data, boolean evaluateWithCrossvalidation) {

		System.out.println(TAG + ": Training... ");
		
		Vector<RevisionHandler> v = new Vector<RevisionHandler>();

		// uses the last attribute as class attribute
		if (data.classIndex() == -1)
			data.setClassIndex(data.numAttributes() - 1);

		// train 
		NaiveBayes cl = new NaiveBayes();
		
		//Options
		try {

		} catch (Exception e) {
			System.err.println(TAG + ": Error invalid ...");
			System.exit(-100);
		}
		
		//Build cluster/classifier
		try {
			//cl.buildClusterer(data);
			cl.buildClassifier(data);
		} catch (Exception e) {
			System.err.println(TAG + ": Error building clusters/classifier.");
			e.printStackTrace();
			System.exit(-101);
		}

		
		// save model + header
		v.add(cl);
		v.add(new Instances(data, 0)); 
		
		System.out.println(TAG + ":... end of Training.");

		if(evaluateWithCrossvalidation)
			evaluate_crossvalidation(cl, data);
		
		return v;
	}

	/*
	 * ************************************************************************************************
	 * ******************************************** Evaluate ******************************************
	 * ************************************************************************************************
	 */

	@Override
	public void evaluate(Vector<RevisionHandler> model, Instances test) {
		
		try {
			// read model and header
			Classifier cl = (Classifier) model.get(0);
			Instances header = (Instances) model.get(1);

			Evaluation eval = new Evaluation(header);
			eval.evaluateModel(cl, test);
			System.out.println(eval.toSummaryString("\nResults\n\n", false));
			
		} catch (Exception e) {
			System.err.println(TAG + ": error training");
			e.printStackTrace();
			System.exit(-102);
		}
		
	}

	@Override
	public void evaluate_crossvalidation(RevisionHandler classifier, Instances data) {
		// TODO Auto-generated method stub
		
		try {
			Evaluation eval = new Evaluation(data);
			eval.crossValidateModel((Classifier)classifier, data, 10, new Random(1));
			String res = eval.toSummaryString("\nResults\n\n", false);
			System.out.println(res);
			Util.wekalog(res);
		} catch (Exception e) {
			System.err.println(TAG + ": error training");
			e.printStackTrace();
			System.exit(-103);
		}	
	}
	

	/*
	 * ************************************************************************************************
	 * ****************************************** Prediction ******************************************
	 * ************************************************************************************************
	 */

	@Override
	public String predict(Vector<RevisionHandler> model, Instances data) {
		System.out.println(TAG + "Predicting...");

		StringBuffer ret = new StringBuffer();
		
		// uses the last attribute as class attribute
		if (data.classIndex() == -1)
			data.setClassIndex(data.numAttributes() - 1);
		
		try {

			// read model and header
			Classifier cl = (Classifier) model.get(0);
			Instances header = (Instances) model.get(1);

			// output predictions
			// Recorremos las instancias
			for (int i = 0; i < data.numInstances(); i++) {
				Instance curr = data.instance(i);

				// predict class
				double pred = cl.classifyInstance(curr); //double pred = cl.clusterInstance(curr);
				String it = curr.stringValue(data.numAttributes() - 1) + " -> " + pred;
				ret.append(it);
				ret.append("\n");
				System.out.println(TAG + ": " + it);
				
			}

			System.out.println(TAG + "... end of prediction.");
		} catch (Exception e) {

			System.err.println(TAG + ": error predicting");
			e.printStackTrace();
			System.exit(-104);
		}

		return ret.toString();
	}

}
