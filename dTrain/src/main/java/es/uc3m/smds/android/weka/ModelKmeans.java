package es.uc3m.smds.android.weka;

import java.util.Vector;

import weka.clusterers.ClusterEvaluation;
import weka.clusterers.Clusterer;
import weka.clusterers.DensityBasedClusterer;
import weka.clusterers.SimpleKMeans;
import weka.core.Debug.Random;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.RevisionHandler;
import es.uc3m.smds.android.utils.Util;


public class ModelKmeans extends ModelInterface{

	private final String TAG;
	
	public ModelKmeans(){
		TAG = "Kmeans";
	}
	
	public String getTag(){
		return TAG;
	}
	
	/*
	 * ************************************************************************************************
	 * ******************************************** Model *********************************************
	 * ************************************************************************************************
	 */
	
	@Override
	public Vector<RevisionHandler> generateModel(Instances data) {
		return generateModel(data, false);
	}
	
	
	@Override
	public Vector<RevisionHandler> generateModel(Instances data, boolean evaluateWithCrossvalidation) {

		System.out.println(TAG + ": Training... ");
		
		Vector<RevisionHandler> v = new Vector<RevisionHandler>();

		// uses the last attribute as class attribute
		//if (data.classIndex() == -1)
			//data.setClassIndex(data.numAttributes() - 1);

		// train k-means
		SimpleKMeans cl = new SimpleKMeans();
		
		//Options
		try {
			cl.setNumClusters(2);
		} catch (Exception e) {
			System.err.println(TAG + ": Error invalid number of clusters.");
			System.exit(-100);
		}

		//Build cluster/classifier
		try {
			cl.buildClusterer(data);
			//cl.buildClassifier(data);
		} catch (Exception e) {
			System.err.println(TAG + ": Error building clusters.");
			e.printStackTrace();
			System.exit(-101);
		}
		
		// save model + header
		v.add(cl);
		v.add(new Instances(data, 0)); 
		
		System.out.println(TAG + ":... end of Training.");


		if(evaluateWithCrossvalidation)
			evaluate_crossvalidation(cl, data);

		return v;
	}
	
	/*
	 * ************************************************************************************************
	 * ******************************************** Evaluate ******************************************
	 * ************************************************************************************************
	 */

	@Override
	public void evaluate(Vector<RevisionHandler> model, Instances test) {
		
		try {
			// read model and header
			Clusterer cl = (Clusterer) model.get(0);
			//Instances header = (Instances) model.get(1);

			ClusterEvaluation eval = new ClusterEvaluation(); 
			eval.setClusterer(cl);
			eval.evaluateClusterer(new Instances(test));
			
			System.out.println(eval.clusterResultsToString());
			
		} catch (Exception e) {
			System.err.println(TAG + ": error training");
			e.printStackTrace();
			System.exit(-102);
		}
		
	}

	@Override
	public void evaluate_crossvalidation(RevisionHandler clusterer, Instances data) {
		
		//only algorithms that implement the interface named DensityBasedClusterer (package weka.clusterers) can be cross-validated
		
		try {
			Vector<RevisionHandler> v = new Vector<RevisionHandler>();
			v.add(clusterer);
			evaluate(v, data);
			double logLikelyhood = ClusterEvaluation.crossValidateModel((DensityBasedClusterer)clusterer, data, 10, new Random(1)); // cross-validate with 10 folds and random number generator with seed 1
			System.out.println("Likelyhood" + logLikelyhood);
			Util.wekalog("Likelyhood " + logLikelyhood);
		} catch (ClassCastException e) {
			System.err.println(TAG + ": only algorithms that implement the interface named DensityBasedClusterer (package weka.clusterers) can be cross-validated");
		} catch (Exception e) {
			System.err.println(TAG + ": error training");
			e.printStackTrace();
			System.exit(-103);
		}	
	}

	
	/*
	 * ************************************************************************************************
	 * ****************************************** Prediction ******************************************
	 * ************************************************************************************************
	 */
	
	@Override
	public String predict(Vector<RevisionHandler> model, Instances data) {
		System.out.println(TAG + "Predicting...");

		StringBuffer ret = new StringBuffer();
		
		try {

			// read model and header
			Clusterer cl = (Clusterer) model.get(0);
			Instances header = (Instances) model.get(1);

			// output predictions
			// Recorremos las instancias
			for (int i = 0; i < data.numInstances(); i++) {
				Instance curr = data.instance(i);
				
				// predict class
				double pred = cl.clusterInstance(curr);
				String it = curr.stringValue(data.numAttributes() - 1) + " -> " + pred;
				ret.append(it);
				ret.append("\n");
				System.out.println(TAG + ": " + it);
			}

			System.out.println(TAG + "... end of prediction.");
		} catch (Exception e) {

			System.err.println(TAG + ": error pedicting");
			e.printStackTrace();
			System.exit(-104);
		}
		
		return ret.toString();

	}

}
