package es.uc3m.smds.android.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import weka.core.Instances;
import weka.core.RevisionHandler;
import weka.core.SerializationHelper;
import weka.core.converters.ArffLoader;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

public class Util {

	//public final static String DIR_HISTOGRAMAS = "/histograms/";
	//public final static String DIR_MODELS = "/models/";


	private static final String TAG = "Util";

	/*
	 * Sirve para copiar el binario strace desde assets a la carpeta DIR_STRACE
	 */
	@SuppressLint("ParserError")
	public static void copyAssetToFilesDir(Context context, String fileName) {
		AssetManager assetManager = context.getAssets();

		File dir = context.getFilesDir();
		File file = new File(dir, fileName);
		InputStream in = null;
		OutputStream out = null;
		try {

			Log.i(TAG, "Copying " + fileName);
			in = assetManager.open(fileName);
			dir.mkdirs(); // por si no existia

			boolean created = file.createNewFile();
			if (created == false) {
				// already exists
				Log.i(TAG, "File already exists. Aborting installation...");
				in.close();
				in = null;
				return;
			}

			out = new FileOutputStream(file);

			byte[] buffer = new byte[1024];
			int read;
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}

			in.close();
			in = null;

			out.flush();
			out.close();
			out = null;

		} catch (Exception e) {
			Log.e(TAG, "Error copying binary " + fileName, e);
		}

	}
	

	/*
	 * Sirve para copiar el binario strace desde assets a la carpeta DIR_STRACE
	 */
	@SuppressLint("ParserError")
	public static void copyAssetToSdcardDir(Context context, String fileName, String toSubdir) {
		AssetManager assetManager = context.getAssets();

		//File dir = context.getFilesDir();
		File dir = new File(Environment.getExternalStorageDirectory(), "/" + toSubdir);
		File file = new File(dir, fileName);
		InputStream in = null;
		OutputStream out = null;
		try {

			Log.i(TAG, "Copying " + fileName);
			in = assetManager.open(fileName);
			dir.mkdirs(); // por si no existia

			boolean created = file.createNewFile();
			if (created == false) {
				// already exists
				Log.i(TAG, "File already exists. Aborting installation...");
				in.close();
				in = null;
				return;
			}

			out = new FileOutputStream(file);

			byte[] buffer = new byte[1024];
			int read;
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}

			in.close();
			in = null;

			out.flush();
			out.close();
			out = null;

		} catch (Exception e) {
			Log.e(TAG, "Error copying binary " + fileName, e);
		}

	}
	
	

	public static Instances loadData(File fTraining_in) {
		
		ArffLoader loader = new ArffLoader();
		try {
			loader.setSource(new FileInputStream(fTraining_in));
		} catch (FileNotFoundException e1) {
			System.err.println("Input file not found: " + fTraining_in);
			System.exit(-2);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(-3);
		}
		
		Instances data = null;
		try {
			data = loader.getDataSet();
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(-4);
		}	
		
		return data;
	}
	

	public static void saveModel(Vector<RevisionHandler> model, File fModel_out) {
		
		try {
			fModel_out.createNewFile();
		} catch (IOException e) {
			System.err.println("Error creating output file form storing the model: " + fModel_out);
			System.exit(-5);
		}
		
		try {
			SerializationHelper.write(new FileOutputStream(fModel_out), model);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening output file form storing the model: " + fModel_out);
			System.exit(-6);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("Error writing output file form storing the model: " + fModel_out);
			e.printStackTrace();
			System.exit(-7);
		}		
	}
	

	public static Vector<RevisionHandler> readModel(File fModel_in) {
		Vector<RevisionHandler> model = null;
		try {
			model = (Vector<RevisionHandler>)SerializationHelper.read(new FileInputStream(fModel_in));
		} catch (FileNotFoundException e) {
			System.err.println("Error opening input file form storing the model: " + fModel_in);
			System.exit(-6);
		}catch (ClassCastException e) {
			System.err.println("Error file doesn't containt a model: " + fModel_in);
			System.exit(-6);
		} catch (Exception e) {
			System.err.println("Error reading input file form storing the model: " + fModel_in);
			e.printStackTrace();
			System.exit(-7);
		}	
		
		return model;
	}


	public static void wekalog(String res) {

		BufferedWriter out = null;
		
    	File working_directory = new File(Environment.getExternalStorageDirectory(), "/StraceExperiments");

		File log = new File(working_directory, "WekaLog.txt");
    	try {
			out = new BufferedWriter(new FileWriter(log.getAbsolutePath(), true));
		} catch (IOException e) {
			Log.e(TAG, "Error preparing file log " + log.getAbsolutePath(), e);
		}
    	
    	try {
			if(!log.exists())
				log.createNewFile();
			
			try {
				out.write(res);
			} catch (IOException e) {
				Log.e(TAG, "Error writing battery stats after receiving bradcast form battery manager.", e);
			}
			
			out.close();
		} catch (IOException e) {
			Log.e(TAG, "Error creating BatteryLog file.", e);
		}
		
	}
	
}
